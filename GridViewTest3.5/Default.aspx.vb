﻿Public Class [Default]
    Inherits System.Web.UI.Page

    Private datasource_grid_a As String() = New String() {"Obj A", "Obj B", "Obj C", "Obj D", "Obj E", "Obj F"}
    Private datasource_grid_b As String() = New String() {"Obj A", "Obj B", "Obj C", "Obj D", "Obj E", "Obj F"}
    Private datasource_grid_c As String() = New String() {"Obj A", "Obj B", "Obj C", "Obj D", "Obj E", "Obj F"}

    private sub Page_init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        AddHandler grid_c.PageIndexChanging, AddressOf grid_test_c_OnPageIndexChanging
        AddHandler grid_c.PageIndexChanged, AddressOf grid_test_c_OnPageIndexChanged
        AddHandler grid_c.Sorting, AddressOf grid_test_c_OnSorting

    End sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        grid_a.DataSource = datasource_grid_a
        grid_b.DataSource = datasource_grid_b
        grid_c.DataSource = datasource_grid_c

        If Not IsPostBack Then
            grid_a.DataBind()
            grid_b.DataBind()
            grid_c.DataBind()
        End If
    End Sub

    ' grid a
    Protected Sub grid_a_OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Dim pageIndexChangingHit = True
        grid_a.PageIndex = e.NewPageIndex
        grid_b.DataBind()
    End Sub

    Protected Sub grid_a_OnPageIndexChanged(sender As Object, e As EventArgs)
        Dim pageIndexChangedHit = True

    End Sub

    Protected Sub grid_a_OnSorting(sender As Object, e As GridViewSortEventArgs)
        Dim pageSortingHit = True
        Dim currentDirection = SortDirection.Descending
        If ViewState("grid_a_direction") IsNot Nothing Then
            currentDirection = ViewState("grid_a_direction")

        End If

        If currentDirection = SortDirection.Ascending Then
            System.Array.Sort(datasource_grid_a)
            ViewState("grid_a_direction") = SortDirection.Descending

        Else
            System.Array.Reverse(datasource_grid_a)
            ViewState("grid_a_direction") = SortDirection.Ascending

        End If
        grid_a.DataBind()

    End Sub

    ' grid b
    Protected Sub grid_test_b_OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grid_b.PageIndexChanging
        Dim pageIndexChangingHit = True
        grid_b.PageIndex = e.NewPageIndex
        grid_b.DataBind()
    End Sub

    Protected Sub grid_test_b_OnPageIndexChanged(sender As Object, e As EventArgs) Handles grid_b.PageIndexChanged
        Dim pageIndexChangedHit = True

    End Sub

    Protected Sub grid_test_b_OnSorting(sender As Object, e As GridViewSortEventArgs) Handles grid_b.Sorting
        Dim pageSortingHit = True
        Dim currentDirection = SortDirection.Descending
        If ViewState("grid_b_direction") IsNot Nothing Then
            currentDirection = ViewState("grid_b_direction")

        End If

        If currentDirection = SortDirection.Ascending Then
            System.Array.Sort(datasource_grid_b)
            ViewState("grid_b_direction") = SortDirection.Descending

        Else
            System.Array.Reverse(datasource_grid_b)
            ViewState("grid_b_direction") = SortDirection.Ascending

        End If
        grid_b.DataBind()

    End Sub

    ' grid c
    Protected Sub grid_test_c_OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Dim pageIndexChangingHit = True
        grid_c.PageIndex = e.NewPageIndex
        grid_c.DataBind()
    End Sub

    Protected Sub grid_test_c_OnPageIndexChanged(sender As Object, e As EventArgs)
        Dim pageIndexChangedHit = True

    End Sub

    Protected Sub grid_test_c_OnSorting(sender As Object, e As GridViewSortEventArgs)
        Dim pageSortingHit = True
        Dim currentDirection = SortDirection.Descending
        If ViewState("grid_c_direction") IsNot Nothing Then
            currentDirection = ViewState("grid_c_direction")

        End If

        If currentDirection = SortDirection.Ascending Then
            System.Array.Sort(datasource_grid_c)
            ViewState("grid_c_direction") = SortDirection.Descending

        Else
            System.Array.Reverse(datasource_grid_c)
            ViewState("grid_c_direction") = SortDirection.Ascending

        End If
        grid_c.DataBind()

    End Sub


End Class