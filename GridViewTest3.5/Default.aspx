﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="GridViewTest3._5.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Grid with events handled by markup</label>
            <asp:GridView ID="grid_a"
                runat="server"
                AutoGenerateColumns="true"
                AllowPaging="true"
                PageSize="3"
                OnPageIndexChanging="grid_a_OnPageIndexChanging"
                OnPageIndexChanged="grid_a_OnPageIndexChanged"
                AllowSorting="True"
                OnSorting="grid_a_OnSorting">
            </asp:GridView>
        </div>
        
        <div>
            <label>Grid with events handled by method handle</label>
            <asp:GridView ID="grid_b"
                runat="server"
                AutoGenerateColumns="true"
                AllowPaging="true"
                PageSize="3"
                AllowSorting="True">
            </asp:GridView>
        </div>
        
        
        <div>
            <label>Grid with events handled programmatically</label>
            <asp:GridView ID="grid_c"
                runat="server"
                AutoGenerateColumns="true"
                AllowPaging="true"
                PageSize="3"
                AllowSorting="True">
            </asp:GridView>
        </div>
    </form>
</body>
</html>
